-- [SECTION] Add New Records
-- Add 5 artists, 2 albums each, 2 songs per album

-- 5 New Artists
INSERT INTO artists (name) VALUES
	("Taylor Swift"),
	("Lady Gaga"),
	("Justin Bieber"),
	("Ariana Grande"),
	("Bruno Mars");

-- 2 Albums for Taylor Swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Fearless", "2008-1-1", 3),
	("Red", "2012-1-1", 3);

-- 2 Songs for Fearless Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Fearless", 246, "Pop rock", 4),
	("State of Grace", 213, "Rock", 4);

-- 2 Songs for Red Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Love Story", 312, "Country pop", 5),
	("Begin Again", 304, "Country", 5);

-- 2 Albums for Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("A Star is Born", "2018-1-1", 4),
	("Born This Way", "2011-1-1", 4);

-- 2 Songs for A Star is Born Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Shallow", 250, "Country, Rock, Folk Rock", 8),
	("Always Remember Us This Way", 310, "Country", 8);

-- 2 Songs for Born This Way Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Bad Romance", 320, "Electro, Pop", 9),
	("Paparazzi", 258, "Pop", 9);


-- Justin Bieber 1 song per album
-- 2 Albums for Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Purpose", "2015-1-1", 5),
	("Believe", "2012-1-1", 5);

-- 1 Song for Purpose Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Sorry", 242, "Dancehall-poptropical", 10);

-- 1 Song for Believe Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Boyfriend", 320, "Pop", 11);


-- Ariana Grande 1 song per album
-- 2 Albums for Ariana Grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("Dangerous Woman", "2016-1-1", 6),
	("Thank U, Next", "2019-1-1", 6);

-- 1 Song for Dangerous Woman Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Into You", 320, "EDM house", 12);

-- 1 Song for Thank U, Next Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Thank U, Next", 246, "Pop, R&B", 13);


-- Bruno Mars 1 song per album
-- 2 Albums for Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES
	("24k Magic", "2016-1-1", 7),
	("Earth to Mars", "2011-1-1", 7);

-- 1 Song for 24k Magic Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("24k Magic", 400, "Funk, Disco, R&B", 14);

-- 1 Song for Earth to Mars Album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
	("Lost", 250, "Pop", 15);




-- [SECTION] Advanced Selection

SELECT * FROM songs WHERE id = 11;

-- exclude
SELECT * FROM songs WHERE id != 11;

SELECT * FROM songs WHERE id < 11;

SELECT * FROM songs WHERE id <= 11;

SELECT * FROM songs WHERE id > 11;

SELECT * FROM songs WHERE id >= 11;

-- Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

-- Combining Conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find Partial Matches
SELECT * FROM songs WHERE song_name LIKE "%e"; -- select keyword from the end

SELECT * FROM songs WHERE song_name LIKE "e%"; -- select keyword from the start

SELECT * FROM songs WHERE song_name LIKE "%e%"; -- select keyword in or in between

SELECT * FROM albums WHERE date_released LIKE "201_-01-01";

SELECT * FROM albums WHERE date_released LIKE "%201%";

-- Sorting Keywords
-- Ascending order
SELECT * FROM songs ORDER BY song_name ASC;

-- Descending order
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting Distinct Records
SELECT DISTINCT genre from songs;

SELECT DISTINCT song_name from songs;


-- [SECTION] Table Joins
-- Combine artist table and albums table

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included in the table
SELECT artists.name, albums.album_title, songs.song_name FROM artists 
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Show artist without records on the right side of the JOIN table
SELECT * FROM artists
LEFT JOIN albums ON artists.id = albums.artist_id;